vault-haproxy
=========

Configures haproxy to manage vault traffic

Requirements
------------

RHEL type system

Chained SSL certificate that contains all the nodes DNS names and IP addresses as a SAN entry

Role Variables
--------------

- **haproxy_node_name:** system name of the haproxy node. Should match whatever is in the Ansible inventory
- **haproxy_ssl_cert:** Chained certificate


Dependencies
------------


Example Playbook
----------------

```yaml
---
- name: Install and configure haproxy for vault
  hosts: all
  become: True  
  vars:
    haproxy_node_name: rproxy
    haproxy_ssl_cert: vault.example.chained.crt
  tasks:
    - name: Configure the haproxy node
      include_role:
        name: vault-haproxy
      when: ansible_hostname == haproxy_node_name
```

License
-------

MIT

Author Information
------------------

Raymond L Cox
